package org.tripsitter;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.h2.H2DatabaseTestResource;
import org.junit.jupiter.api.Test;
import org.tripsitter.Substance;
import org.tripsitter.dto.TripDTO;
import org.tripsitter.logic.TripLogic;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;


@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
public class TripResourceTest {

    @Inject
    TripLogic testLogic;

    @Test
    public void dontAddTrip_When_BadName() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "";
        testTrip.description = "a";

        given()
                .body(testTrip)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "dontAddTrip_When_BadName")
                .when().post("/trips")
                .then()
                .statusCode(401);
    }

    @Test
    public void dontAddTrip_When_BadDescription() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "a";
        testTrip.description = "8WzL9VWjCehYnq4OWb3oYv0wT14tXnEZ3YYPAOFOfGOdLrqomQpZkrBtFPZCT4aZV68n7LCEr0XWIacYSH33zKztWQzMUMAm0dos5jWSlpbkjRNbDkXGDeQxpyDHliuSocOPs9DNlvL5UzACK3sJLFEl5gYJrY81xbVWBXr5ZNFRnRFcT7YAX4Pj7hdTubLLQCqm0ueR9mckdK9Y0jvWWVa1Sw6GjLTcGEN3Z4oUWa4kKlTcjIoAV24zRo3xrEhJS";

        given()
                .body(testTrip)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "dontAddTrip_When_BadDescription")
                .when().post("/trips")
                .then()
                .statusCode(401);
    }

    @Test
    public void testAddTrip() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "good";
        testTrip.description = "good";
        testTrip.substance = Substance.shrooms;

        given()
                .body(testTrip)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .when().post("/trips")
                .then()
                .statusCode(200);
    }

    @Test
    public void dontUpdateTrip_When_BadDescription() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "a";
        testTrip.description = "8WzL9VWjCehYnq4OWb3oYv0wT14tXnEZ3YYPAOFOfGOdLrqomQpZkrBtFPZCT4aZV68n7LCEr0XWIacYSH33zKztWQzMUMAm0dos5jWSlpbkjRNbDkXGDeQxpyDHliuSocOPs9DNlvL5UzACK3sJLFEl5gYJrY81xbVWBXr5ZNFRnRFcT7YAX4Pj7hdTubLLQCqm0ueR9mckdK9Y0jvWWVa1Sw6GjLTcGEN3Z4oUWa4kKlTcjIoAV24zRo3xrEhJS";

        given()
                .body(testTrip)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "dontUpdateTrip_When_BadDescription")
                .when().put("/trips")
                .then()
                .statusCode(401);
    }

    @Test
    public void dontUpdateTrip_When_NotYours() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "good";
        testTrip.description = "good";
        testTrip.substance = Substance.shrooms;
        testTrip = testLogic.addTrip(testTrip, "NOT_dontUpdateTrip_When_NotYours");

        given()
                .body(testTrip)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "dontUpdateTrip_When_NotYours")
                .when().put("/trips")
                .then()
                .statusCode(401);
    }

    @Test
    public void dontUpdateTrip_When_BadName() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "";
        testTrip.description = "a";

        given()
                .body(testTrip)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "dontUpdateTrip_When_BadName")
                .when().post("/trips")
                .then()
                .statusCode(401);
    }

    @Test
    public void testUpdateTrip() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "good";
        testTrip.description = "good";
        testTrip.substance = Substance.shrooms;
        testTrip = testLogic.addTrip(testTrip, "dontUpdateTrip_When_NotYours");

        given()
                .body(testTrip)
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "dontUpdateTrip_When_NotYours")
                .when().put("/trips")
                .then()
                .statusCode(200);
    }

    @Test
    public void dontDeleteTrip_When_NotYours() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "good";
        testTrip.description = "good";
        testTrip.substance = Substance.shrooms;
        testTrip = testLogic.addTrip(testTrip, "NOT_dontDeleteTrip_When_NotYours");

        given()
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "dontDeleteTrip_When_NotYours")
                .pathParam("tripId", testTrip.id)
                .when().delete("/trips/{tripId}")
                .then()
                .statusCode(401);
    }

    @Test
    public void testDeleteTrip() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "good";
        testTrip.description = "good";
        testTrip.substance = Substance.shrooms;
        testTrip = testLogic.addTrip(testTrip, "testDeleteTrip");

        given()
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "testDeleteTrip")
                .pathParam("tripId", testTrip.id)
                .when().delete("/trips/{tripId}")
                .then()
                .statusCode(200);
    }

    @Test
    public void testGetTrip() {
        TripDTO testTrip = new TripDTO();
        testTrip.latitude = 0;
        testTrip.longitude = 0;
        testTrip.name = "good";
        testTrip.description = "good";
        testTrip.substance = Substance.shrooms;
        testTrip = testLogic.addTrip(testTrip, "testGetTrip");

        given()
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .header( "uid", "testGetTrip")
                .pathParam("tripId", testTrip.id)
                .when().get("/trips/{tripId}")
                .then()
                .statusCode(200);
    }
}
