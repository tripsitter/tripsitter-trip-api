package mock;

import io.quarkus.test.Mock;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.tripsitter.services.SitService;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@Mock
@RestClient
@ApplicationScoped
public class SitServiceMock implements SitService {

    @Override
    public List<Long> getSittings(String userId) {
        return null;
    }

    @Override
    public void deleteSitsForTrip(long tripId) {

    }
}
