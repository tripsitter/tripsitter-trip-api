package org.tripsitter;

public enum Substance {
    LSD,
    shrooms,
    truffels,
    mdma,
    DMT,
    salvia,
    keta,
    cb
}
