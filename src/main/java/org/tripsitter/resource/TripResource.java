package org.tripsitter.resource;

import org.tripsitter.dto.FinderOptions;
import org.tripsitter.dto.TripDTO;
import org.tripsitter.logic.TripLogic;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

@Path("/trips")
@Produces(MediaType.APPLICATION_JSON)
public class TripResource {

    @Inject
    TripLogic tripLogic;

    @POST
    public Response addTrip(@HeaderParam("uid") String userId, TripDTO tripDTO) {
        try {
            return Response.ok(tripLogic.addTrip(tripDTO, userId)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(401).build();
        }
    }

    @PUT
    public Response updateTrip(@HeaderParam("uid") String userId, TripDTO tripDTO) {
        try {
            tripLogic.updateTrip(tripDTO, userId);
            return Response.status(200).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

    @DELETE
    @Path("/{tripId}")
    public Response deleteTrip(@HeaderParam("uid") String userId, @PathParam("tripId") long tripId) {
        try {
            tripLogic.deleteTrip(tripId, userId);
            return Response.status(200).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

    @GET
    @Path("/{tripId}")
    public Response getTrip(@PathParam("tripId") long tripId) {
        try {
            return Response.ok(tripLogic.getTrip(tripId)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(401).build();
        }
    }

    @GET
    @Path("/all/{page}")
    public Response getTrips(@HeaderParam("uid") String userId, @PathParam("page") int page) {
        try {
            return Response.ok(tripLogic.getTrips(userId, page)).build();
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/finder/{page}")
    public Response findTrips(@HeaderParam("uid") String userId, @PathParam("page") int page, FinderOptions options) {
        // todo move to its own api and add options param
        try {
            return Response.ok(tripLogic.findTrips(userId, page, options)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/info")
    public Response tripInfo(Set<Long> tripIds) {
        try {
            return Response.ok(tripLogic.tripInfo(tripIds)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(401).build();
        }
    }
}
