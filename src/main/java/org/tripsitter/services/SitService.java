package org.tripsitter.services;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.tripsitter.dto.TripDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.*;
import java.util.List;
import java.util.Set;

@Path("/sits")
@RegisterRestClient
@ApplicationScoped
public interface SitService {

    @GET
    @Path("/sitting")
    List<Long> getSittings(@HeaderParam("uid") String userId);

    @DELETE
    @Path("/trip/{tripId}")
    void deleteSitsForTrip(@PathParam("tripId") long tripId);
}
