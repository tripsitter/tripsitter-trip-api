package org.tripsitter.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity(name = "trip")
public class TripEntity extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String userId;

    public String name;

    public String description;

    public String substance;

    public boolean open;

    public double longitude;

    public double latitude;

    public Date created;
}
