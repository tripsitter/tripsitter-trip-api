package org.tripsitter.logic;

import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.tripsitter.dto.FinderOptions;
import org.tripsitter.dto.TripDTO;
import org.tripsitter.entity.TripEntity;
import org.tripsitter.services.SitService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Transactional
@ApplicationScoped
public class TripLogic {

    @Inject
    @RestClient
    SitService sitService;

    public TripDTO addTrip(TripDTO tripDTO, String userId) {
        if (invalidName(tripDTO.name) || invalidDescription(tripDTO.description) || !canAddTrip(userId)) {
            throw new RuntimeException();
        }
        TripEntity trip = new TripEntity();
        trip.description = tripDTO.description;
        trip.name = tripDTO.name;
        trip.substance = tripDTO.substance.name();
        trip.longitude = tripDTO.longitude;
        trip.latitude = tripDTO.latitude;
        trip.open = true;
        trip.userId = userId;
        trip.created = new Date();
        trip.persist();
        return tripEntityToTripDTO(trip);
    }

    public List<TripDTO> getTrips(String userId, int page) {
        return TripEntity.find("userId = :userId", Parameters.with("userId", userId))
                .page(page, 20)
                .stream()
                .map(TripEntity.class::cast)
                .map(this::tripEntityToTripDTO)
                .collect(Collectors.toList());
    }

    public TripDTO getTrip(long tripId) {
        Optional<TripEntity> trip = TripEntity.findByIdOptional(tripId);
        trip.orElseThrow(RuntimeException::new);
        return tripEntityToTripDTO(trip.get());
    }

    public void updateTrip(TripDTO tripDTO, String userId) {
        TripEntity trip = TripEntity.findById(tripDTO.id);
        if (invalidName(tripDTO.name) || invalidDescription(tripDTO.description) || !trip.userId.equals(userId)) {
            throw new RuntimeException();
        }
        if (trip.open) {
            trip.substance = tripDTO.substance.name();
            trip.name = tripDTO.name;
            trip.description = tripDTO.description;
            trip.longitude = tripDTO.longitude;
            trip.latitude = tripDTO.latitude;
        }
        trip.open = tripDTO.open;
    }

    public void deleteTrip(long tripId, String userId) {
        TripEntity tripEntity = TripEntity.findById(tripId);
        if (!tripEntity.userId.equals(userId)) {
            throw new RuntimeException();
        }
        tripEntity.delete();
        sitService.deleteSitsForTrip(tripId);
    }

    public List<TripDTO> findTrips(String userId, int page, FinderOptions options) {
        Map<String, Object> tripParameters = new HashMap<>();

        // todo move to other service, query by rating

        tripParameters.put("userId", userId);
        tripParameters.put("latitude", options.latitude);
        tripParameters.put("longitude", options.longitude);
        tripParameters.put("distance", options.distance);
        tripParameters.put("substances", options.substances.stream().map(Enum::name).collect(Collectors.toList()));

        List<Long> sittings = sitService.getSittings(userId);
        if (sittings.size() == 0) {
            sittings.add((long) 0);
        }
        tripParameters.put("trips", sittings);

        return TripEntity.find("userId != :userId and open = true and id not in :trips and substance in :substances and 111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(latitude)) * COS(RADIANS(:latitude)) * COS(RADIANS(longitude - :longitude)) + SIN(RADIANS(latitude)) * SIN(RADIANS(:latitude))))) <= :distance", Sort.descending("created"), tripParameters)
                .page(Page.of(page, 12))
                .stream()
                .map(TripEntity.class::cast)
                .map(this::tripEntityToTripDTO)
                .collect(Collectors.toList());
    }

    public List<TripDTO> tripInfo(Set<Long> ids) {
        return ids.stream().map(i -> tripEntityToTripDTO(TripEntity.findById(i))).collect(Collectors.toList());
    }

    private boolean canAddTrip(String userId) {
        return TripEntity.find("userId = :userId", Parameters.with("userId", userId)).count() <= 9;
    }

    private TripDTO tripEntityToTripDTO(TripEntity tripEntity) {
        return new TripDTO(tripEntity.id, tripEntity.name, tripEntity.description, tripEntity.substance, tripEntity.open, tripEntity.longitude, tripEntity.latitude, tripEntity.userId);
    }

    private boolean invalidName(String name) {
        return name.length() <= 0 || name.length() > 32;
    }

    private boolean invalidDescription(String description) {
        return description.length() > 256;
    }
}
