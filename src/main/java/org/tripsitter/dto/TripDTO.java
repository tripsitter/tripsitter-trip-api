package org.tripsitter.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.tripsitter.Substance;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TripDTO {

    public long id;

    public String name;

    public String userId;

    public String description;

    public Substance substance;

    public boolean open;

    public double longitude;

    public double latitude;

    public TripDTO(){

    }

    public TripDTO(long id, String name, String description, String substance, boolean open, double longitude, double latitude, String userId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.substance = Substance.valueOf(substance);
        this.open = open;
        this.longitude = longitude;
        this.latitude = latitude;
        this.userId = userId;
    }
}
