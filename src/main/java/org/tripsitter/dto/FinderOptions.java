package org.tripsitter.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.tripsitter.Substance;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FinderOptions {
    public double distance;
    public Set<Substance> substances;
    public int rating;
    public double longitude;
    public double latitude;
}
